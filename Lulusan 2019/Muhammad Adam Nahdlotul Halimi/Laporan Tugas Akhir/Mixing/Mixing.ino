const int buttonPin = 3;     // the number of the pushbutton pin
const int rotorPin =  8;      
const int rotorPin2 = 12;
// variables will change:
int buttonState = 0;         // variable for reading the pushbutton status

void setup() {
 
  pinMode(rotorPin, OUTPUT);
  pinMode(rotorPin2, OUTPUT);
  pinMode(buttonPin, INPUT);
}

void loop(){
  
  buttonState = digitalRead(buttonPin);

  if (buttonState == HIGH) {
    digitalWrite(rotorPin, HIGH);
    //delay(20000);
    //digitalWrite(rotorPin, LOW);
    digitalWrite(rotorPin2, HIGH);
    //delay(60000);
 }
  else {
    digitalWrite(rotorPin, LOW);
    delay(10000);
    digitalWrite(rotorPin, HIGH);
    digitalWrite(rotorPin2, LOW);
    delay(60000);
  }
}
